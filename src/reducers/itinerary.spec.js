/* eslint-env jasmine */
import itineraryReducer from './itinerary';
import { ItineraryActions } from '../actions/ItineraryActions';

describe('Itinerary reducer', () => {
  it('returns initial state by default', () => {
    expect(itineraryReducer(undefined, { type: 'init' }).places).toEqual([]);
  });

  it('ADD_PLACE action inserts place as last element', () => {
    const state = {
      places: [{ title: 'Tiergarten' }],
    };
    const action = {
      type: ItineraryActions.ADD_PLACE,
      payload: {
        place: { title: 'Alexanderplatz' },
      },
    };

    const expectedStatePlaces = [...state.places, action.payload.place];
    expect(itineraryReducer(state, action).places).toEqual(expectedStatePlaces);
  });

  describe('REMOVE_PLACE action', () => {
    it('removes place if exists', () => {
      const state = {
        places: [{ title: 'Tiergarten' }, { title: 'Alexanderplatz' }],
      };
      const action = {
        type: ItineraryActions.REMOVE_PLACE,
        payload: {
          place: state.places[0],
        },
      };

      const expectedStatePlaces = [state.places[1]];
      expect(itineraryReducer(state, action).places).toEqual(expectedStatePlaces);
    });

    it('makes no changes to state if place not found', () => {
      const state = {
        places: [{ title: 'Tiergarten' }, { title: 'Alexanderplatz' }],
      };
      const action = {
        type: ItineraryActions.REMOVE_PLACE,
        payload: {
          place: { title: 'Mitte' },
        },
      };

      expect(itineraryReducer(state, action).places).toEqual(state.places);
    });
  });

  describe('MOVE_UP_PLACE action', () => {
    it('moves place one position up', () => {
      const state = {
        places: [{ title: 'Tiergarten' }, { title: 'Alexanderplatz' }],
      };
      const action = {
        type: ItineraryActions.MOVE_UP_PLACE,
        payload: {
          place: state.places[1],
        },
      };

      const expectedStatePlaces = [state.places[1], state.places[0]];
      expect(itineraryReducer(state, action).places).toEqual(expectedStatePlaces);
    });

    it('makes no changes if place is first element', () => {
      const state = {
        places: [{ title: 'Tiergarten' }, { title: 'Alexanderplatz' }],
      };
      const action = {
        type: ItineraryActions.MOVE_UP_PLACE,
        payload: {
          place: state.places[0],
        },
      };

      expect(itineraryReducer(state, action).places).toEqual(state.places);
    });
  });

  describe('MOVE_DOWN_PLACE action', () => {
    it('moves place one position down', () => {
      const state = {
        places: [{ title: 'Tiergarten' }, { title: 'Alexanderplatz' }],
      };
      const action = {
        type: ItineraryActions.MOVE_DOWN_PLACE,
        payload: {
          place: state.places[0],
        },
      };

      const expectedStatePlaces = [state.places[1], state.places[0]];
      expect(itineraryReducer(state, action).places).toEqual(expectedStatePlaces);
    });

    it('makes no changes if place is last element', () => {
      const state = {
        places: [{ title: 'Tiergarten' }, { title: 'Alexanderplatz' }],
      };
      const action = {
        type: ItineraryActions.MOVE_DOWN_PLACE,
        payload: {
          place: state.places[1],
        },
      };

      expect(itineraryReducer(state, action).places).toEqual(state.places);
    });
  });
});
