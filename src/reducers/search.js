import { SearchActions } from '../actions/SearchActions';

const INITIAL_STATE = {
  query: '',
  results: [],
};

export default function searchReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SearchActions.SEARCH_RESULT:
      return {
        query: action.payload.query,
        results: action.payload.results,
      };
    case SearchActions.CLEAN_SEARCH:
      return INITIAL_STATE;
    default:
      return state;
  }
}
