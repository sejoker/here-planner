import { combineReducers } from 'redux';
import itinerary from './itinerary';
import search from './search';

export const rootReducer = combineReducers({
  search,
  itinerary,
});
