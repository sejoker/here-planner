import { ItineraryActions } from '../actions/ItineraryActions';

const INITIAL_STATE = {
  places: [],
};

const swapPlaces = (leftIndex, rightIndex, places) => {
  const newPlaces = [...places];
  const temp = newPlaces[leftIndex];
  newPlaces[leftIndex] = newPlaces[rightIndex];
  newPlaces[rightIndex] = temp;
  return newPlaces;
};

export default function itineraryReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case ItineraryActions.ADD_PLACE: {
      const newState = {
        places: [...state.places],
      };
      newState.places.push(action.payload.place);
      return newState;
    }
    case ItineraryActions.REMOVE_PLACE: {
      const indexToRemove = state.places.indexOf(action.payload.place);

      if (indexToRemove === -1) return state;

      const newState = {
        places: [...state.places],
      };
      newState.places.splice(indexToRemove, 1);
      return newState;
    }
    case ItineraryActions.MOVE_UP_PLACE: {
      const indexToMove = state.places.indexOf(action.payload.place);

      if (indexToMove === 0) {
        return state;
      }

      const newState = {
        places: [...swapPlaces(indexToMove, indexToMove - 1, state.places)],
      };
      return newState;
    }
    case ItineraryActions.MOVE_DOWN_PLACE: {
      const indexToMove = state.places.indexOf(action.payload.place);

      if (indexToMove === state.places.length - 1) {
        return state;
      }

      const newState = {
        places: [...swapPlaces(indexToMove, indexToMove + 1, state.places)],
      };
      return newState;
    }
    default:
      return state;
  }
}
