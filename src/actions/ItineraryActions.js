import { NgRedux } from 'ng2-redux';
import { Injectable } from '@angular/core';

@Injectable()
export class ItineraryActions {
  static ADD_PLACE = 'ADD_PLACE';
  static REMOVE_PLACE = 'REMOVE_PLACE';
  static MOVE_UP_PLACE = 'MOVE_UP_PLACE';
  static MOVE_DOWN_PLACE = 'MOVE_DOWN_PLACE';

  constructor(ngRedux: NgRedux) {
    this.ngRedux = ngRedux;
  }

  addPlace(place) {
    this.ngRedux.dispatch({
      type: ItineraryActions.ADD_PLACE,
      payload: {
        place,
      },
    });
  }

  removePlace(place) {
    this.ngRedux.dispatch({
      type: ItineraryActions.REMOVE_PLACE,
      payload: {
        place,
      },
    });
  }

  moveUpPlace(place) {
    this.ngRedux.dispatch({
      type: ItineraryActions.MOVE_UP_PLACE,
      payload: {
        place,
      },
    });
  }

  moveDownPlace(place) {
    this.ngRedux.dispatch({
      type: ItineraryActions.MOVE_DOWN_PLACE,
      payload: {
        place,
      },
    });
  }

}
