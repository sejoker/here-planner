import { NgRedux } from 'ng2-redux';
import { Injectable } from '@angular/core';
import { searchPlaces } from '../utils/helpers';

@Injectable()
export class SearchActions {
  static SEARCH_RESULT = 'SEARCH_RESULT';
  static CLEAN_SEARCH = 'CLEAN_SEARCH';

  constructor(ngRedux: NgRedux) {
    this.ngRedux = ngRedux;
  }

  search(query, config) {
    searchPlaces(query, config)
      .then(data => {
        this.ngRedux.dispatch({
          type: SearchActions.SEARCH_RESULT,
          payload: {
            query,
            results: data.slice(0, 5),
          },
        });
      });
  }

  cleanSearch() {
    this.ngRedux.dispatch({
      type: SearchActions.CLEAN_SEARCH,
    });
  }
}
