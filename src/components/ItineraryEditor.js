import { Component } from '@angular/core';
import { NgRedux } from 'ng2-redux';
import { ItineraryActions } from '../actions/ItineraryActions';

@Component({
  selector: 'itinerary',
  providers: [ItineraryActions],
  styles: [`
    .itp__itinerary-container {
      padding: 0 5px;
    }
  `],
  template: `
  <div class="itp__itinerary-container">
    <ul class="itp__itinerary-list collection" *ngFor="let item of places" >
        <li>
          <a class="btn-floating right red" (click)="actions.removePlace(item)">
            <i class="tiny material-icons">delete</i>
          </a>
          <a class="btn-floating right" (click)="actions.moveDownPlace(item)">
            <i class="tiny material-icons">call_received</i>
          </a>
          <a class="btn-floating right" (click)="actions.moveUpPlace(item)">
            <i class="tiny material-icons">call_made</i>
          </a>
          <div class="collection-item">{{item.title}}</div>
        </li>
    </ul>
  </div>
  `,
})
export default class ItineraryEditor {
  constructor(
    actions: ItineraryActions,
    ngRedux: NgRedux) {
    this.actions = actions;
    this.ngRedux = ngRedux;
    this.places = [];
  }

    /** @internal component lifecycle hook */
  ngOnInit() {
    this.itinerary$ = this.ngRedux.select(['itinerary', 'places']);
    this.itinerary$.subscribe(places => {
      this.places = [...places];
    });
  }
}
