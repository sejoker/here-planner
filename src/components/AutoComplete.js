import { Component, ElementRef } from '@angular/core';
import { NgRedux } from 'ng2-redux';
import { FormControl } from '@angular/forms';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/debounceTime';
import { SearchActions } from '../actions/SearchActions';
import { ItineraryActions } from '../actions/ItineraryActions';
import PlatformConfigProvider from '../providers/PlatformConfigProvider';

/* eslint-disable max-len */
@Component({
  selector: 'autocomplete',
  providers: [SearchActions, ItineraryActions, PlatformConfigProvider],
  host: {
    '(document:click)': 'handleClick($event)',
  },
  styles: [`
      .itp__suggestions {
          position:absolute;
          width:300px;
          background: white;
          z-index: 10;
      }

      .itp__suggestions ul {
          padding: 0px;
          margin: 0px;
      }

      .itp__ac-container {
          width:300px;
      }

      .itp__ac-field {
        padding: 5px;
        margin: 0;
      }
      .itp__ac-field > input {
        margin: 0;
      }
      .itp__suggestions ul li {
          list-style: none;
          padding: 0px;
          margin: 0px;
      }

      .itp__suggestions ul li a {
          padding:5px;
          display: block;
          text-decoration: none;
          color:#7E7E7E;
      }

      .itp__suggestions ul li a:hover {
          background-color: #f1f1f1;
      }
  `],
  template: `
      <div class="itp__ac-container" >
          <div class="input-field itp__ac-field">
            <input type="text" placeholder="Search ..." class="validate filter-input" [(formControl)]=queryObservable>
          </div>
          <div class="itp__suggestions" *ngIf="suggestions.length > 0">
              <ul *ngFor="let item of suggestions" >
                  <li>
                      <a (click)="select(item)">{{item.title}}</a>
                  </li>
              </ul>
          </div>
      </div>
    `,
})
export default class AutoComplete {

  constructor(
    myElement: ElementRef,
    searchActions: SearchActions,
    itineraryActions: ItineraryActions,
    platformConfig: PlatformConfigProvider,
    ngRedux: NgRedux) {
    this.elementRef = myElement;
    this.suggestions = [];
    this.queryObservable = new FormControl();
    this.queryObservable.valueChanges
      // limit number calls to API
      .debounceTime(400)
      .distinctUntilChanged()
      .subscribe(query => {
        this.query = query;
        this.search();
      });

    this.searchActions = searchActions;
    this.itineraryActions = itineraryActions;
    this.platformConfig = platformConfig;
    this.ngRedux = ngRedux;
  }

  ngOnInit() {
    this.searchResults$ = this.ngRedux.select(['search', 'results']);
    this.searchResults$.subscribe(res => {
      this.suggestions = res;
    });
  }

  search() {
    this.searchActions.search(this.query, this.platformConfig);
  }

  select(place) {
    this.itineraryActions.addPlace(place);
    this.searchActions.cleanSearch();
    this.queryObservable.setValue('');
  }

  handleClick(event) {
    let clickedComponent = event.target;
    let inside = false;
    do {
      if (clickedComponent === this.elementRef.nativeElement) {
        inside = true;
      }
      clickedComponent = clickedComponent.parentNode;
    } while (clickedComponent);
    if (!inside) {
      this.suggestions = [];
    }
  }
}
