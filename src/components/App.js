import { Component } from '@angular/core';

@Component({
  selector: 'here-planner',
  styles: [`
    .itp__search-panel {
      position: absolute;
      z-index: 1;
      background: white;
      margin: 10px;
    }
    .itp__container {
      position: relative;
    }
  `],
  template: `
    <div itp__container>
      <div class="container">
        <div class="itp__search-panel col s12 m6 l3">
          <autocomplete></autocomplete>
          <itinerary></itinerary>
        </div>
      </div>
      <map-view></map-view>
    </div>
  `,
})
export default class App {
}
