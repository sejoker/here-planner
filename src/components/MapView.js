/* global H */

import { Component, ElementRef } from '@angular/core';
import { NgRedux } from 'ng2-redux';
import PlatformConfigProvider from '../providers/PlatformConfigProvider';

@Component({
  selector: 'map-view',
  styles: [`
    .itp__map-container {
      width: 100%;
      height: 100%;
      position: absolute;
    }
  `],
  template: `
    <div class='itp__map-container'>
    </div>
  `,
  providers: [PlatformConfigProvider],
})
export default class MapView {
  constructor(
    platformConfig: PlatformConfigProvider,
    elem: ElementRef,
    ngRedux: NgRedux) {
    this.platformConfig = platformConfig;
    this.elem = elem;
    this.ngRedux = ngRedux;
    this.mapInitialized = false;
  }

  /** @internal component lifecycle hook */
  ngOnInit() {
    // we need access to DOM element
    const container = this.elem.nativeElement.querySelector('.itp__map-container');

    this.itinerary$ = this.ngRedux.select(['itinerary', 'places']);
    this.itinerary$.subscribe(waypoints => {
      this.initMap(container, waypoints);
    });

    this.initMap(container);
  }

  initMap(elem, waypoints) {
    if (!this.mapInitialized) {
      // initialize communication with the platform
      this.platform = new H.service.Platform(this.platformConfig);
      const defaultLayers = this.platform.createDefaultLayers();

      const mapSettings = {
        zoom: 12,
        // set center to Berlin
        center: { lat: this.platformConfig.lat, lng: this.platformConfig.lng },
      };
      // initialize a map  - not specificing a location will give a whole world view.
      this.map = new H.Map(elem, defaultLayers.normal.map, mapSettings);
      // make the map interactive
      // MapEvents enables the event system
      // Behavior implements default interactions for pan/zoom (also on mobile touch environments)
      this.behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(this.map));

      // Create the default UI components
      this.ui = H.ui.UI.createDefault(this.map, defaultLayers);

      this.mapInitialized = true;
    } else {
      // clear existing objects
      this.map.removeObjects(this.map.getObjects());
    }

    if (waypoints && waypoints.length > 1) {
      // Create the parameters for the routing request:
      const routingParameters = {
        // The routing mode:
        mode: 'fastest;car',
        // To retrieve the shape of the route we choose the route
        // representation mode 'display'
        representation: 'display',
      };
      for (let i = 0; i < waypoints.length; i++) {
        routingParameters[`waypoint${i}`] =
          `${waypoints[i].position[0]},${waypoints[i].position[1]}`;
      }

      // Define a callback function to process the routing response:
      const onResult = (result) => {
        if (result.response.route) {
          // Pick the first route from the response:
          const route = result.response.route[0];
          // Pick the route's shape:
          const routeShape = route.shape;

          // Create a strip to use as a point source for the route line
          const strip = new H.geo.Strip();

          // Push all the points in the shape into the strip:
          routeShape.forEach(point => {
            const parts = point.split(',');
            strip.pushLatLngAlt(parts[0], parts[1]);
          });

          // Retrieve the mapped positions of the requested waypoints:
          const markers = route.waypoint.map(waypoint => new H.map.Marker({
            lat: waypoint.mappedPosition.latitude,
            lng: waypoint.mappedPosition.longitude,
          }));

          // Create a polyline to display the route:
          const routeLine = new H.map.Polyline(strip, {
            style: { strokeColor: 'blue', lineWidth: 5 },
            arrows: { fillColor: 'white', frequency: 2, width: 0.8, length: 0.7 },
          });

          // Add the route polyline and the two markers to the map:
          this.map.addObjects([routeLine, ...markers]);

          // Set the map's viewport to make the whole route visible:
          this.map.setViewBounds(routeLine.getBounds());
        }
      };
      // Get an instance of the routing service:
      const router = this.platform.getRoutingService();

      // Call calculateRoute() with the routing parameters,
      // the callback and an error callback function (called if a
      // communication error occurs):
      router.calculateRoute(routingParameters, onResult,
        (error) => {
          /* eslint-disable no-console */
          console.error(error.message);
        });
    }
  }
}
