/* eslint-env jasmine */
import { TestBed } from '@angular/core/testing';
import { NgRedux } from 'ng2-redux';
import ItineraryEditor from './ItineraryEditor';
import { ItineraryActions } from '../actions/ItineraryActions';


describe('ItineraryEditor', () => {
  it('renders empty list', () => {
    TestBed.configureTestingModule({
      declarations: [ItineraryEditor],
      providers: [ItineraryActions, NgRedux],
    });

    const fixture = TestBed.createComponent(ItineraryEditor);
    fixture.detectChanges();

    // not really useful, just check we could render component in tests
    expect(fixture.debugElement.nativeElement.textContent.trim()).toEqual('');
  });
});
