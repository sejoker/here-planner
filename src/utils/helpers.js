import axios from 'axios';

export function searchPlaces(query, config) {
  return axios.get(`https://places.cit.api.here.com/places/v1/autosuggest?app_id=${config.app_id}&app_code=${config.app_code}&at=${config.lat},${config.lng}&q=${query}&result_types=address,place`)
      .then(response => response.data.results.map(place => ({
        title: place.title,
        position: place.position,
      })));
}
