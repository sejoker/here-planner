export default class PlatformConfigProvider {
  constructor() {
    this.app_id = 'yjgTV7F5oIqdYLJznOV0';
    this.app_code = '_kvTSZ7MKxoDgq6Bmq0x-g';
    // configure all services to use the 'CIT' environment
    this.useCIT = true;
    // configure to communicate with the platform via HTTPS
    this.useHTTPS = true;
    // set center to Berlin
    this.lat = 52.5159;
    this.lng = 13.3777;
    // don't allow changes
    Object.freeze(this);
  }
}

