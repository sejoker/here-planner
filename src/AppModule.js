import { NgModule, enableProdMode } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import 'rxjs/add/operator/map';
import { NgRedux } from 'ng2-redux';
import createLogger from 'redux-logger';
import { rootReducer } from './reducers';
import App from './components/App';
import ItineraryEditor from './components/ItineraryEditor';
import MapView from './components/MapView';
import AutoComplete from './components/AutoComplete';

enableProdMode();

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    App,
    MapView,
    AutoComplete,
    ItineraryEditor,
  ],
  providers: [
    NgRedux,
  ],
  bootstrap: [App],
})
export class AppModule {
  constructor(ngRedux: NgRedux) {
    ngRedux.configureStore(rootReducer, {}, [createLogger()]);
  }
}
