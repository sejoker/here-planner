# HERE Web Coding Challenge

## Decisions

I decided to give a try to Angular2. I found that Angular 2 embraced the component-based approach which worked excellently with React. The framework reached 2.0.0-rc.6 recently that brings hope we will have final version this year. I also decided to build
application data flows with Redux and ng2-redux library. The benefit is to have loosely connected components and ability to test reducers/actions separately from UI layer. I chose Babel with ES6 over TypeScript. It's easy to add types with Flow later if needed.
Angular2 uses Rx Observables as communication pattern which is different from React (you can add them, but it's not mandatory to use). Observables make writing tests a little bit more complicated that's why I have only one dummy component test. I covered reducers logic with unit tests; it was straightforward and useful to do (I spot one bug on the way).

I chose (babel-angular2-app)[https://github.com/shuhei/babel-angular2-app] starter kit for setting up the application. It uses Babel and Browserify out of the box.

I used Materialize framework for CSS only for the reason of quick wins it provides. I am not a big fan of massive CSS projects and will think twice before using it in production other than for the internal tools. I prefer more lightweight CSS frameworks like (BASSCSS)[http://www.basscss.com/] to build own company's style guides.


I think I implemented all the required user stories and hit  two optional points (tests and responsive design). This is my thought as for other not implemented features:
- Sharing routes to other users. It requires serializing selected waypoints and initialization logic for the application to parse them from the query parameters. Redux store allows being initialized with the initial state.
- Choose transport type for the mode of transport. It will require adding transport reducer/actions into the store. After reading the selected value from the store we will provide it to routing service.

## Demo

http://here-planner.surge.sh/

### Install

```
npm install
```

### Build

Build once:

```
npm run build
```

Watch files and rebuild:

```
npm run watch
# or
npm start
```

### Preview

```
npm run preview
```

### Lint

```
npm run lint
```

### Test

```
npm test
```

## License

[ISC](https://opensource.org/licenses/ISC)
